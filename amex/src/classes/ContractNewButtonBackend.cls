public with sharing class ContractNewButtonBackend {
    
    public Contract contract1{get;set;}
    public List<wrapperClause> wrapperClausesList{get;set;}
    public List<wrapperClause> selectedclauses{get;set;}
    private List<ClauseToContract__c> contractContains{get;set;}
  	private String soql {get;set;}
    public boolean visible {get;set;}
    public boolean saveButtonRender{get;set;}
    private string ContractID{get;set;}
   
    public ContractNewButtonBackend(ApexPages.StandardSetController controller){
        //Get the current Contract id 
        ContractID=ApexPages.currentPage().getParameters().get('id');
        //This is to show the Contract number on the VF page
        contract1 = [select id,ContractNumber from Contract where Id = :ContractID limit 1];
        //Query up all juntions that the object has currently
        contractContains = [select Clause__c from ClauseToContract__c where Contract__c = :ContractID];
        //Create new instances of variables
        soql = 'select Name, Type__c,id from Clauses__c where Name != null';
        selectedClauses = new List<wrapperClause>();
        visible = false;
        saveButtonRender = false;
        //Begin the actual search for the initial list
    	runQuery();
        
        controller.setPageSize(10);
    }
    public void runQuery() {

    try {
      //limiting the search from the database to 20 to not hit governer limits  
      List<Clauses__c> queryList = Database.query(soql + ' limit 20');  
        
        wrapperClausesList = new List<wrapperClause>();
        Map<Id,Clauses__c> index = new Map<Id,Clauses__c>();
        
        if(queryList !=null){
            index.putAll(queryList);
            
            for(ClauseToContract__c ctc :contractContains){
                index.remove(ctc.Clause__c);
            }
            if(!selectedClauses.isEmpty()){
                for(wrapperClause wc:selectedClauses){
                    index.remove(wc.clause.id);
                }
            }
            for(Clauses__c c:index.values()){
                wrapperClausesList.add(new wrapperClause(c));
            }
        }
    }
     catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong, Please Refresh!'));
    }

  }
    public PageReference runSearch() {

    String ClauseName = Apexpages.currentPage().getParameters().get('ClauseName');
    String ClauseType = Apexpages.currentPage().getParameters().get('ClauseType');
    
    soql = 'select Name, Type__c,id  from Clauses__c where Name != null';
    if (!ClauseName.equals(''))
      soql += ' and Name LIKE '+'\'%'+String.escapeSingleQuotes(ClauseName)+'%\'';
    if (!ClauseType.equals(''))
      soql += ' and Type__c LIKE'+'\'%'+String.escapeSingleQuotes(ClauseType)+'%\'';
            
    runQuery();
    return null;

    }
    public PageReference processSelected(){
      boolean check = false;
      Map<Id,wrapperClause> index = new Map<Id,wrapperClause>();   
      
        for(wrapperClause wrapperClauseObject : wrapperClausesList){
          index.put(wrapperClauseObject.clause.id, wrapperClauseObject);}
        
        if(selectedClauses.isEmpty()){
            for(wrapperClause wc: index.values()){
                if(wc.selected==true){
                    check = true;
                selectedClauses.add(wc);}
            } 
        }
        else{
            for(wrapperClause wc:selectedClauses){
               index.remove(wc.clause.id); 
            }
            for(wrapperClause wc: index.values()){
                if(wc.selected==true){
                selectedClauses.add(wc);}
        	}
        }
    	
        if(check){
            visible = true;
            saveButtonRender = true;
        }
      return null;  
    }
    public PageReference removeSelected(){
        Integer indexs = 0;
        
        for(wrapperClause wc: selectedClauses){
            if (wc.selected == false){
                break;
            }
            indexs++;
        }
        if(!selectedClauses.isEmpty()){
            selectedClauses.remove(indexs);
        }
        if(selectedClauses.isEmpty()){
            visible = false;
            saveButtonRender = false;
        }
    return null;    
    }
    public class wrapperClause{
        public Clauses__c clause{get;set;}
        public Boolean selected {get;set;}
        
        public wrapperClause(Clauses__c c){
            clause = c;
            selected = false;
        }
    }
    public PageReference save(){
        ClauseToContract__c[] connection =new List<ClauseToContract__c>();
        try{
            for(wrapperClause wc:selectedClauses){
                ClauseToContract__c ctc = new ClauseToContract__c(Clause__c = wc.clause.id, Contract__c = ContractID);
                connection.add(ctc);
            }
            insert connection;
        }catch (Exception e) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not Save, Please Refresh and Try again.'));
            }
        
        PageReference back = new PageReference('/'+ContractID);    
        return back;
    }
    public PageReference cancel(){
        PageReference back = new PageReference('/'+ContractID);    
        return back;
    }
}