@isTest(SeeAllData=true)
public class TestClassAddButton {	
    public static testMethod void testMyController() {
        List<ClauseToContract__c> ctcs = new List<ClauseToContract__c>();
        List<ContractNewButtonBackend.wrapperClause> cList = new List<ContractNewButtonBackend.wrapperClause>();
         
        Contract testContract =  new Contract(AccountId = '001f400000PNVZ0', Status = 'Draft', StartDate = date.newInstance(2018, 5, 21), ContractTerm = 10);
        insert testContract;
        Clauses__c testClause = new Clauses__c(Name = 'New Clause 1', Type__c = 'Financial' );
        insert testClause;
        
        ContractNewButtonBackend.wrapperClause cnbbwc = new ContractNewButtonBackend.wrapperClause(testClause);
        cList.add(cnbbwc);
        
        Clauses__c testClause2 = new Clauses__c(Name = 'New Clause 2', Type__c = 'Financial' );
        insert testClause2;
        
        ContractNewButtonBackend.wrapperClause cnbbwc2 = new ContractNewButtonBackend.wrapperClause(testClause2);
        cList.add(cnbbwc2);
        
        Clauses__c testClause3 = new Clauses__c(Name = 'New Clause 3', Type__c = 'Financial' );
        insert testClause3;
        
        ContractNewButtonBackend.wrapperClause cnbbwc3 = new ContractNewButtonBackend.wrapperClause(testClause3);
        cList.add(cnbbwc3);
        
        ClauseToContract__c testConnection = new ClauseToContract__c(Clause__c = testClause3.id, Contract__c = testContract.id);
        insert testConnection;
        
        PageReference pageRef = Page.ContractNewButton;
        pageRef.getParameters().put('id', testContract.id);
        pageRef.getParameters().put('ClauseName','n');
        pageRef.getParameters().put('ClauseType','f');
        Test.setCurrentPage(pageRef);
        
        Apexpages.StandardSetController ssc = new Apexpages.StandardSetController(ctcs);
        ContractNewButtonBackend cnbb = new ContractNewButtonBackend(ssc);
        
        cnbb.runQuery();
        //Apex queries up whats in the database and whats in this instance
        system.assert(cList.size() <= cnbb.wrapperClausesList.size());
        
        cnbb.runsearch();
        cnbb.processSelected();
        cnbb.removeSelected();
        cnbb.save();
        cnbb.cancel();
        
		cnbbwc2 = new ContractNewButtonBackend.wrapperClause(testClause2);
        cnbbwc2.selected = true;
        cnbb.wrapperClausesList.add(cnbbwc2);
        cnbb.processSelected();
        
        cnbbwc3 = new ContractNewButtonBackend.wrapperClause(testClause3);
        cnbbwc3.selected = true;
        cnbb.wrapperClausesList.add(cnbbwc3);
        cnbb.processSelected();
        
        cnbbwc = new ContractNewButtonBackend.wrapperClause(testClause);
        cnbb.selectedclauses.add(cnbbwc);
        cnbb.removeSelected();
        cnbb.runQuery();
        cnbb.save();
        
        cnbb.selectedclauses = null;
        cnbb.runQuery();
        cnbb.save();
        
    }
}